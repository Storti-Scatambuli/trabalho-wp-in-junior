<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <script src="script.js"></script>
    <script>pegaLobos()</script>
    <title>Adote um Lobinho</title>
    <?php wp_head(); ?>
</head>
<body onload="mostraLobinhosIndex()">
    <header>
        <div class="position">
            <a href="<?php echo get_stylesheet_directory_uri() ?>/quemsomos.php" class="quemSomos">Quem Somos</a>
            <a href="<?php echo get_stylesheet_directory_uri() ?>/index.php"><img src="<?php echo get_stylesheet_directory_uri() ?>/media/icon.png" class="icon"></a>
            <a href="<?php echo get_stylesheet_directory_uri() ?>/listalobos.php" class="lobinhos">Nossos Lobinhos</a>
        </div>
    </header>