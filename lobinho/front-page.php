<?php 
// Template Name: Pagina inicial
?>

<?php get_header(); ?>
    <main>
        <section class="background">
            <div class="ajuste">
                <picture><img src="<?php echo get_stylesheet_directory_uri() ?>/media/image.png"></picture>
                <label class="frase1"><?php the_field('topo_titulo_inicial') ?></label>
                <label class="frase2"><?php the_field('topo_descricao_inicial') ?></label>
            </div>
            <div class="sobre">
                <h2 class="titSobre"><?php the_field('sobre_titulo_sobre') ?></h2>
                <p class="pSobre"><?php the_field('sobre_descricao_sobre') ?></p>
            </div>
            <div class="valores">
                <h2 class="titValores">Valores</h2>
                <picture class="containImg">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/Protection.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/care.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/company.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/rescue.png" class="valor">
                </picture>
                <div class="mother">
                    <div class="oldest">
                        <h3><?php the_field('valores_titulo_1') ?></h3>
                        <p><?php the_field('valores_descricao_1') ?></p>
                    </div>
                    <div class="older">
                        <h3><?php the_field('valores_titulo_2') ?></h3>
                        <p><?php the_field('valores_descricao_2') ?></p>
                    </div>
                    <div class="younger">
                        <h3><?php the_field('valores_titulo_3') ?></h3>
                        <p><?php the_field('valores_descricao_3') ?></p>
                    </div> 
                    <div class="youngest">
                        <h3><?php the_field('valores_titulo_4') ?></h3>
                        <p><?php the_field('valores_descricao_4') ?></p>
                    </div>
                </div>
            </div>
            <div class="exemplos">
                <h2 class="titExemplos">Lobos Exemplo</h2>
                <?php 
                    $the_query = new WP_Query('posts_per_page=2')
                ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <div class="geralEsq">
                    <?php if( get_field('lobos_foto') ): ?>
                    <div class="ladoEsq">
                        <div class="imgEsq">
                            <img src="<?php the_field('lobos_foto'); ?>" />
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="infoEsq">
                        <h4 class="titEsq"><?php the_field('lobos_nome') ?></h4>
                        <h5 class="idadeEsq"><?php the_field('lobos_idade') ?></h5>
                        <p class="fraseEsq"><?php the_field('lobos_descricao') ?></p>
                    </div>
                </div>
                <br><br>
                <?php endwhile; else: ?>
                <p>desculpe, o post não segue os critérios escolhidos</p>
                <?php endif; ?>
            </div>
        </section>
    </main>
<?php get_footer(); ?>