<?php 
// Template Name: Lobinhos
?>

<?php get_header(); ?>
    <!--campo de pesquisa-->
    <div class="campoPesquisa">
        <picture ><img src="media/busca.png" class="lupa" onclick="mostraLobinhos()"></picture>
        <input type="text" class="pesquisa"> 
        <button type="button" id="addLobo"><a href="adicionarlobo.html">+ Lobo</a></button>
    </div>
    <br>
   <div>
        <input type="checkbox" id="lobinhosadotados" onclick="mostraLobinhos()">
        <label>Ver lobinhos adotados</label>
    </div>
    <br><br>
    <!--lobos-->

    <section class="lobo">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="geralEsq">
                    <?php if( get_field('lobos_foto') ): ?>
                    <div class="ladoEsq">
                        <div class="imgEsq">
                            <img src="<?php the_field('lobos_foto'); ?>" />
                        </div>
                    </div>
                    <?php endif; ?>
        <div class="infoEsq">
            <div class="loboEsq">
                <div>
                    <h4 id="normal"><?php the_field('lobos_nome') ?></h4>
                    <h5 id="normal"><?php the_field('lobos_idade') ?></h5>
                </div>
                <button class="btnEsq" id=adotar type="button"><a href="showlobo.php" onclick="pegaIdLobin(this.id))">ADOTAR</a></button>
            </div>
                <p class="fraseEsq"><?php the_field('lobos_descricao') ?></p></div></div><br><br>
    </section>
    <?php endwhile; else: ?>
    <p>desculpe, o post não segue os critérios escolhidos</p>
    <?php endif; ?>

<?php get_footer(); ?>